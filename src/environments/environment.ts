// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCL6glNkwNgiAB-ZaouoTldf4qdDlCcf3g",
    authDomain: "desafio-ripley-5f44b.firebaseapp.com",
    databaseURL: "https://desafio-ripley-5f44b.firebaseio.com",
    projectId: "desafio-ripley-5f44b",
    storageBucket: "desafio-ripley-5f44b.appspot.com",
    messagingSenderId: "1036229464746",
    appId: "1:1036229464746:web:17022a192eb18b229076d3",
    measurementId: "G-0LL448C3VJ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
