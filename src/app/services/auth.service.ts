import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user$ = new BehaviorSubject<undefined | firebase.User>(undefined);

  constructor(private afAuth: AngularFireAuth) {
    this.userState();
  }

  async emailPassLogin(email: string, password: string) {
    const sesion = await this.afAuth.signInWithEmailAndPassword(email, password);
    return sesion
  }

  async emailPassCreate(email: string, password: string) {
    const registro = await this.afAuth.createUserWithEmailAndPassword(email, password);
    return registro
  }

  userState() {
    this.afAuth.onAuthStateChanged(firebaseUser => { console.log({ firebaseUser }) })
  }

  logOut() {
    this.afAuth.signOut();
  }

  profile() {
    this.afAuth.authState
  }


}
