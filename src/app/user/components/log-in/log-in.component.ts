import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { IErrorAuthResponse } from '../../../interface/angularFireAuth.interface'
import { MatSnackBar } from '@angular/material';

@Component({
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {

  form: FormGroup;
  public loginInvalid: boolean;
  public msg: string;
  public msgError: string;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private _auth: AuthService,
    private _snackBar: MatSnackBar
  ) {
  }

  async ngOnInit() {
    this._auth.userState();
    this.form = this.fb.group({
      username: ['', Validators.email],
      password: ['', Validators.required]
    });
  }

  async onLogIn() {
    this.loginInvalid = false;
    if (this.form.valid) {
      try {
        const username: string = this.form.get('username').value;
        const password: string = this.form.get('password').value;
        const promesa = this._auth.emailPassLogin(username, password);
        promesa
          .then(res => {
            this.msgError = '';
            this.router.navigate(['home']);
          })
          .catch((err: IErrorAuthResponse) => this.msgError = err.message);

      } catch (err) {
        this.loginInvalid = false;
      }
    } else {
      this.loginInvalid = true;
      this.msg = ' The username and password were not recognised';
    }
  }

  async onSignIn() {
    this.loginInvalid = false;
    if (this.form.valid) {
      try {
        const username = this.form.get('username').value;
        const password = this.form.get('password').value;
        const promesa = this._auth.emailPassCreate(username, password);
        promesa
          .then(res => {
            this.msgError = '';
            this.openSnackBar('Usuario creado con exito', 'Crear')
          })
          .catch((err: IErrorAuthResponse) => this.msgError = err.message)

      } catch (err) {
        this.loginInvalid = false;
      }
    } else {
      this.loginInvalid = true;
      this.msg = ' The username and password were not recognised';
    }
  }

  msgEmail() {
    const state = this.form.get('username').valid;
    this.loginInvalid = !state;
    (this.loginInvalid && this.form.get('username').value !== '') ? this.msg = ' Please provide a valid email address' : this.msg = '';
  }

  msgPass() {
    const state = this.form.get('password').valid;
    this.loginInvalid = !state;
    (this.loginInvalid && this.form.get('username').value !== '') ? this.msg = ' Please provide a valid password' : this.msg = '';
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
