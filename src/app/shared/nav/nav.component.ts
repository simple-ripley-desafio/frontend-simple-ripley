import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  constructor(private s_auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  logOut() {
    this.s_auth.logOut();
    this.router.navigate(['user']);
  }

}
