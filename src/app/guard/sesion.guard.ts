import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
    providedIn: 'root'
})
export class SesionGuard implements CanActivate {
    constructor(private router: Router, private _auth: AuthService) { }

    canActivate(): boolean {
        const user = this._auth.user$.value;
        console.log({ user })
        const validation = (user !== undefined && user !== null) ? true : false;
        validation ? this.router.navigate(['/home']) : this.router.navigate(['/user']);
        return validation;
    }
}
