import { Directive, Output, EventEmitter, HostListener } from '@angular/core';


@Directive({
  selector: '[appHover]'
})
export class HoverDirective {
  @Output() hoverEvent = new EventEmitter<boolean>();


  constructor() { }

  @HostListener('mouseenter') onMouseEnter() {
    this.hoverEvent.emit(true)
  }
}
