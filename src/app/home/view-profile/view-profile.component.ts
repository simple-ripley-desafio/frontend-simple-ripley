import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HeroOrVillain } from '../interfaces/HeroOrVillain.interface';

@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.scss']
})
export class ViewProfileComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ViewProfileComponent>, @Inject(MAT_DIALOG_DATA) public data: HeroOrVillain) { }

  ngOnInit() {
    
  }

  close_dialog(): void {
    this.dialogRef.close();
  }



}
