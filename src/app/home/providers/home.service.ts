import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HeroOrVillain } from '../interfaces/HeroOrVillain.interface';
import { HttpClient } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class HomeService {

  private heroes: HeroOrVillain[] = [
    {
      avatar: 'https://picsum.photos/id/618/200/300',
      battlesWon: 1000,
      category: 'Hero',
      city: 'Metrópolis',
      mision: true,
      name: 'Clark Kent',
      skill: ['Super fuerza', 'Visión lazer', 'Piel de acero']
    },
    {
      avatar: 'assets/img/avatarBatman.jpg',
      battlesWon: 800,
      category: 'Hero',
      city: 'Gotham',
      mision: true,
      name: 'Bruce Wayne',
      skill: ['Millonario', 'Tecnologia única', 'Artes Marciales']
    },
    {
      avatar: 'assets/img/avatarWonderWoman.jpg',
      battlesWon: 800,
      category: 'Hero',
      city: 'Amazonas',
      mision: true,
      name: 'Princes Diana',
      skill: ['Super Fuerza', 'Inmortalidad', 'Tacticas de Guerra']
    },
    {
      avatar: 'assets/img/avatarThor.jpg',
      battlesWon: 800,
      category: 'Hero',
      city: 'Asgard',
      mision: true,
      name: 'Thor Odinson',
      skill: ['Mjolnir', 'Dios del Trueno', 'Tacticas de Guerra']
    },
    {
      avatar: 'assets/img/avatarGroot.jpg',
      battlesWon: 800,
      category: 'Hero',
      city: 'Planeta X',
      mision: true,
      name: 'Groot',
      skill: ['Regeneracion', 'Control de Vegetación', 'Cambia Formas']
    }
  ]


  private heroes$: BehaviorSubject<HeroOrVillain[]> = new BehaviorSubject<HeroOrVillain[]>([]);

  constructor(private _http: HttpClient) {
    this.heroes$.next(this.heroes)
  }

  getHeroes = () => {
    return this.heroes$.asObservable()
  }

  getImg() {
    return this._http.get('https://picsum.photos/id/618/200/300');
  }

}
