import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { SharedModule } from '../shared/shared.module';
import { MaterialModule } from '../Material/material.module';
import { HomeService } from './providers/home.service';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ViewProfileComponent } from './view-profile/view-profile.component';
import { FormsProfileComponent } from './forms-profile/forms-profile.component';
import { HoverDirective } from '../directives/hover.directive';
import { ListHeroesComponent } from './list-heroes/list-heroes.component';


@NgModule({
  declarations: [HomeComponent, ViewProfileComponent, FormsProfileComponent, HoverDirective, ListHeroesComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,
    MaterialModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [HomeService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [ViewProfileComponent, FormsProfileComponent]
})
export class HomeModule { }
