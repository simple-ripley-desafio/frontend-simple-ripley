import { Component, OnInit, Input } from '@angular/core';
import { Observable, } from 'rxjs';
import { HeroOrVillain } from '../interfaces/HeroOrVillain.interface';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ViewProfileComponent } from '../view-profile/view-profile.component';
import { HomeService } from '../providers/home.service';
import { tap, map } from 'rxjs/operators';

@Component({
  selector: 'app-list-heroes',
  templateUrl: './list-heroes.component.html',
  styleUrls: ['./list-heroes.component.scss']
})
export class ListHeroesComponent implements OnInit {

  @Input() heroes: Observable<HeroOrVillain[]>

  constructor(private dialog: MatDialog, private _home: HomeService) { }

  ngOnInit() {
  }

  OpenProfile = (hero: HeroOrVillain): void => {
    this._home.getImg().pipe(
      map((data) => console.log(data)),
      tap(data => console.log(data))
    );
    const config: MatDialogConfig = {

    }
    this.dialog.open(ViewProfileComponent, {
      height: '600px',
      width: '400px',
      data: hero
    });


  }


}
