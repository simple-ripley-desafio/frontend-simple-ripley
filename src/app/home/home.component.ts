import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { HeroOrVillain } from './interfaces/HeroOrVillain.interface';
import { HomeService } from './providers/home.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  heroes: Observable<HeroOrVillain[]>

  constructor(private s_home: HomeService) { }

  ngOnInit(): void {
    this.loadHeroes()
  }


  private loadHeroes(): void {
    this.heroes = this.s_home.getHeroes()
  }



}
